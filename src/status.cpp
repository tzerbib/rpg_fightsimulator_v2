#include "status.h"

Status::Status(std::string name, int duration, std::string note){
  this->setName(name);
  this->setDuration(duration);
  this->setNote(note);
}

std::string Status::getName() const{
  return this->name;
}

void Status::setName(std::string n){
  this->name = n;
}

int Status::getDuration() const{
  return this->duration;
}

void Status::setDuration(int d){
  this->duration = d;
}

std::string Status::getNote() const{
  return this->note;
}

void Status::setNote(std::string n){
  this->note = n;
}
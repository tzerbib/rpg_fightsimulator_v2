#include "monster.h"

Monster::Monster(int lp, std::vector<Status> status){
  this->setLife(lp);
  this->setStatus(status);
}


int Monster::getLife() const{
  return this->life;
}


void Monster::setLife(int lp){
  this->life = lp;
}


const std::vector<Status>* Monster::getStatus() const{
  return &this->status;
}


void Monster::setStatus(std::vector<Status> s){
  this->status = s;
}
#include "event.h"

Event::Event(std::string name, int time, int init, std::string note){
  this->setName(name);
  this->setTime(time);
  this->setInit(init);
  this->setNote(note);
}

std::string Event::getName() const{
  return this->name;
}

void Event::setName(std::string n){
  this->name = n;
}

int Event::getTime() const{
  return this->time;
}

void Event::setTime(int t){
  this->time = t;
}

int Event::getInit() const{
  return this->init;
}

void Event::setInit(int i){
  this->init = i;
}

std::string Event::getNote() const{
  return this->note;
}

void Event::setNote(std::string n){
  this->note = n;
}

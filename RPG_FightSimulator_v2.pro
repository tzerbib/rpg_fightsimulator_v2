QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG += c++11

# Generation of a GUI application
TEMPLATE += app
CONFIG += windows

# Define name of the executable
TARGET = RPG_FightSimulator_v2

# The following define makes your compiler emit warnings if you use
# any Qt feature that has been marked deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0


INCLUDEPATH += \
  $$PWD/include

SOURCES += \
  $$files($$PWD/src/*.cpp, true)

HEADERS += \
  $$files($$PWD/include/*.h, true)

TRANSLATIONS += \
  $$files($$PWD/translate/*.ts, true)


RESOURCES += \
  $$files($$PWD/ressources.qrc, true)


release:DESTDIR = bin/release
release:OBJECTS_DIR = obj/release
release:MOC_DIR = moc/release
release:RCC_DIR = rcc/release
release:UI_DIR = ui/release

debug:DESTDIR = bin/debug
debug:OBJECTS_DIR = obj/debug
debug:MOC_DIR = moc/debug
debug:RCC_DIR = rcc/debug
debug:UI_DIR = ui/debug



# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

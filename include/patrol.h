#ifndef PATROL_H
#define PATROL_H

#include <string>
#include <vector>
#include "monster.h"

/*! \class Patrol
 * \brief A monster patrol.
 *
 * This class represent a monster patrol.
*/
class Patrol{
  // Attributes
  protected:
    std::string name; /* The patrol name */
    int init; /* The patrol initiative */
    std::vector<Monster> members; /* Members constituting the patrol */

  // Methods
  public:

    /*!
     * \brief Patrol class contructor.
     *
     * \param name Name of the patrol.
     * \param init Initiative of the patrol.
     * \param monsters Members of the patrol.
    */
    Patrol(std::string name, int init, std::vector<Monster> monsters);

    /*!
     * \brief Getter of attribute name.
     *
     * \return The name of the patrol.
    */
    std::string getName() const;

    /*!
     * \brief Setter of attribute name.
     *
     * \param n The name of the patrol.
    */
    void setName(std::string n);

    /*!
     * \brief Getter of attribute init.
     *
     * \return The init of the patrol.
    */
    int getInit() const;

    /*!
     * \brief Setter of attribute init.
     *
     * \param i The init of the patrol.
    */
    void setInit(int i);

    /*!
     * \brief Getter of attribute members.
     *
     * \return A vector containing members of the patrol.
    */
    const std::vector<Monster>* getMembers() const;

    /*!
     * \brief Setter of attribute members.
     *
     * \param m A vector of members of the patrol.
    */
    void setMembers(std::vector<Monster> m);
};


// Operator

// Patrols are compared according to their initiative.
inline bool operator< (const Patrol& lhs, const Patrol& rhs){
  return lhs.getInit() < rhs.getInit();
}

inline bool operator> (const Patrol& lhs, const Patrol& rhs){
  return rhs < lhs;
}

#endif // PATROL_H
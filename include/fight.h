#ifndef FIGHT_H
#define FIGHT_H

#include "player.h"
#include "patrol.h"
#include "event.h"


/*! \class Fight
 * \brief The representation of the fight.
 *
 * This class represent the fight between adventurers and monster patrols.
*/
class Fight{
  // Attributes
  protected:
    int round; /* The round number */
    int init; /* The current initiative in the round */
    std::vector<Player> players; /* All player involved in the fight */
    std::vector<Player>::const_iterator nextPlayer;/* Iterator on next player */
    int numberPlayers; /* Number of players playing. */
    std::vector<Patrol> patrols; /* All patrols involved in the fight */
    int numberPatrols; /* Number of patrols playing. */
    std::vector<Patrol>::const_iterator nextPatrol;/* Iterator on next partols*/
    std::vector<Event> events; /* Events that will take place during the fight*/

  // Methods
  public:
    /*!
     * \brief Fight class contructor.
     *
     * \param p Players involved in the fight.
     * \param m Patrols involved in the fight.
     * \param m Events that will take place during the fight.
    */
    Fight(std::vector<Player> p, std::vector<Patrol> m, std::vector<Event> e);


    /*!
     * \brief Getter of attribute round.
     *
     * \return The current round of the fight.
    */
    int getRound() const;

    /*!
     * \brief Setter of attribute round.
     *
     * \param r The curent round of the fight.
    */
    void setRound(int r);


    /*!
     * \brief Getter of attribute init.
     *
     * \return The current init of the round.
    */
    int getInit() const;

    /*!
     * \brief Setter of attribute init.
     *
     * \param i The curent init of the round.
    */
    void setInit(int i);


    /*!
     * \brief Getter of attribute players.
     *
     * \return All players involved in the fight.
    */
    const std::vector<Player>* getPlayers() const;

    /*!
     * \brief Setter of attribute players.
     *
     * \param p All players involved in the fight.
    */
    void setPlayers(std::vector<Player> p);


    /*!
     * \brief Getter of attribute nextPlayer.
     *
     * \return The iterator on the next player who will play.
    */
    const std::vector<Player>::const_iterator getNextPlayer() const;

    /*!
     * \brief Setter of attribute nextPlayer.
     *
     * \param it The iterator on the last player who will play.
    */
    void setNextPlayer(std::vector<Player>::const_iterator it);


    /*!
     * \brief Getter of attribute numberPlayers.
     *
     * \return Numbers of players who are playing.
    */
    int getNumberPlayers() const;

    /*!
     * \brief Setter of attribute numberPlayers.
     *
     * \param np Numbers of players who are playing.
    */
    void setNumberPlayers(int np);


    /*!
     * \brief Getter of attribute patrols.
     *
     * \return All patrols involved in the fight.
    */
    const std::vector<Patrol>* getPatrols() const;

    /*!
     * \brief Setter of attribute patrols.
     *
     * \param p All patrols involved in the fight.
    */
    void setPatrols(std::vector<Patrol> p);


    /*!
     * \brief Getter of attribute nextPatrol.
     *
     * \return The iterator on the next patrol who is playing.
    */
    const std::vector<Patrol>::const_iterator getNextPatrol() const;

    /*!
     * \brief Setter of attribute nextPatrol.
     *
     * \param it The iterator on the next patrol who is playing.
    */
    void setNextPatrol(std::vector<Patrol>::const_iterator it);


    /*!
     * \brief Getter of attribute numberPatrols.
     *
     * \return Numbers of patrols who are playing.
    */
    int getNumberPatrols() const;

    /*!
     * \brief Setter of attribute numberPatrols.
     *
     * \param np Numbers of patrols who are playing.
    */
    void setNumberPatrols(int np);


    /*!
     * \brief Getter of attribute events.
     *
     * \return All events that will take place during the fight.
    */
    const std::vector<Event>* getEvents() const;

    /*!
     * \brief Setter of attribute events.
     *
     * \param e Events that will take place during the fight.
    */
    void setEvents(std::vector<Event> e);


    /*!
     * \brief Function which goes to the next turn.
     *
     * \return The init of battlers of the next turn in the round.
     * 
     * This function goes to next turn by decreasing the initiative
     * of the current round.
    */
    int nextTurn();


  protected:

    /*!
     * \brief Function which goes to the next round.
     *
     * \return The round number.
    */
    int newRound();

    /*!
     * \brief Function which returns the highest initiative among all fighter.
     * 
     * return The highest initiative among all fighter.
    */
    int getHighestInit() const;
};


// Operator

struct greater
{
  template<class T> bool operator()(T const &a, T const &b) const {
    return a > b;
  }
};

#endif // FIGHT_H

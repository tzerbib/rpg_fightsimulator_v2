#ifndef EVENT_H
#define EVENT_H

#include <string>

/*! \class Event
 * \brief event that could occur during a fight.
 *
 * This class represent some event during a fight.
 */

class Event {

  // Attributes
  protected:
    std::string name;
    int time;
    int init;
    std::string note;

  // Methods
  public:

    /*!
     * \brief Event class contructor.
     *
     * \param name Name of the event.
     * \param time Time remaining before the event.
     * \param init Init when the event happens.
     * \param note Message to send to the user.
    */
    Event(std::string name, int time, int init, std::string note);


    /*!
     * \brief Getter of attribute name.
     *
     * \return The name of the event.
    */
    std::string getName() const;


    /*!
     * \brief Setter of attribute name.
     *
     * \param n The name of the event.
    */
    void setName(std::string n);


    /*!
     * \brief Getter of attribute time.
     *
     * \return The time remaining before the event.
    */
    int getTime() const;


    /*!
     * \brief Setter of attribute time.
     *
     * \param t The time remaining before the event.
    */
    void setTime(int t);


    /*!
     * \brief Getter of attribute init.
     *
     * \return Init when the event append.
    */
    int getInit() const;


    /*!
     * \brief Setter of attribute init.
     *
     * \param i Init when the event append.
    */
    void setInit(int i);


    /*!
     * \brief Getter of attribute note.
     *
     * \return The message to send to the user.
    */
    std::string getNote() const;


    /*!
     * \brief Setter of attribute note.
     *
     * \param n A string of the message to send to the user.
    */
    void setNote(std::string n);
};


// Operator

/*
 * Events are compared according to their round and then initiative :
 * 'time' is looked first, then 'init' (from highest to lowest except 0 which is
 * always highest)
 * So an event with 0 as init happens always before an events with the same 
 * 'time' but a non-zero 'init'.
*/
inline bool operator< (const Event& lhs, const Event& rhs){
  return (lhs.getTime() < rhs.getTime())
      || (lhs.getTime() == rhs.getTime()
          && (lhs.getInit() == 0
          || (rhs.getInit() != 0 && lhs.getInit() > rhs.getInit())));
}

inline bool operator> (const Event& lhs, const Event& rhs){
  return rhs < lhs;
}


#endif // EVENT_H

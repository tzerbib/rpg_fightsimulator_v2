#ifndef MONSTER_H
#define MONSTER_H

#include <vector>
#include "status.h"

/*! \class Monster
 * \brief A monster in the fight.
 *
 * This class represent a monster.
*/
class Monster{
  // Attributes
  protected:
    int life; /* The monster life*/
    std::vector<Status> status; /* All status affecting the monster */

  // Methods
  public:

    /*!
     * \brief Monster class contructor.
     *
     * \param lp Initial monster life points.
     * \param status Status list of the monster.
    */
    Monster(int lp, std::vector<Status> status);

    /*!
     * \brief Getter of attribute life.
     *
     * \return The life of the monster.
    */
    int getLife() const;

    /*!
     * \brief Setter of attribute life.
     *
     * \param lp The life of the monster.
    */
    void setLife(int lp);

    /*!
     * \brief Getter of attribute status.
     *
     * \return All monster status.
    */
    const std::vector<Status>* getStatus() const;

    /*!
     * \brief Setter of attribute status.
     *
     * \param s A vector of status.
    */
    void setStatus(std::vector<Status> s);
};


#endif // MONSTER_H